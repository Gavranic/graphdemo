﻿using GraphDemo.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDemo
{
    class MainWindowViewModel : BindableBase
    {

        private DataViewModel _dataViewModel = new DataViewModel();
        private GraphViewModel _graphViewModel = new GraphViewModel();

        private BindableBase _currentViewModel;
        public BindableBase CurrentViewModel {
            get { return _currentViewModel; }
            set { SetProperty(ref _currentViewModel, value); }
        }
        public MainWindowViewModel()
        {
            DataCommand = new RelayCommand(DataShow);
            GraphCommand = new RelayCommand(GraphShow);
            CurrentViewModel = _dataViewModel;
        }

        public RelayCommand DataCommand { get; private set; }
        public RelayCommand GraphCommand { get; private set; }

        private void DataShow()
        {
            CurrentViewModel = _dataViewModel;
        }

        private void GraphShow()
        {
            if (CurrentViewModel is DataViewModel)
            {
                var model = CurrentViewModel as DataViewModel;
                if(model.FileName!=null)
                    _graphViewModel.FileName = model.FileName.Content.ToString();
            }
            CurrentViewModel = _graphViewModel;
        }

    }
}
