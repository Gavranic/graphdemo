﻿using GraphDemo.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDemo.Services
{
    class DataRepository
    {
        public List<Data> GetData(string fileName)
        {
            List<Data> data = new List<Data>();

            string path1  = Directory.GetParent(System.AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName;
            path1 += "\\Data\\" + fileName + ".txt";

            string[] lines = File.ReadAllLines(path1);

            foreach(var line in lines)
            {
                var array= line.Split(new string[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);
                NumberFormatInfo info = new NumberFormatInfo();
                info.NumberDecimalSeparator = ".";
                data.Add(new Data { Lat = Convert.ToDouble(array[1],info), Lgn = Convert.ToDouble(array[2],info), Time = Convert.ToDouble(array[0], info), Velocity = Convert.ToDouble(array[3], info) });
            }
            
            return data;
        }
    }
}
