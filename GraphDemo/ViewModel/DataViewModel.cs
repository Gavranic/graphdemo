﻿using GraphDemo.Model;
using GraphDemo.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GraphDemo.ViewModel
{
    class DataViewModel : BindableBase
    {
        private DataRepository rep = new DataRepository();

        private ListBoxItem fileName;
        public ListBoxItem FileName {
            get { return fileName; }
            set
            {
                SetProperty(ref fileName, value);
                DataSet = new ObservableCollection<Data>(rep.GetData(fileName.Content.ToString()));
            }
        }

        private ObservableCollection<Data> dataSet;
        public ObservableCollection<Data> DataSet
        {
            get {
                return dataSet;
            }
            set
            {
                SetProperty(ref dataSet,value);
            }
        }
        
    }
}
