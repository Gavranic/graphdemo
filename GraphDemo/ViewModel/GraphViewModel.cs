﻿using GraphDemo.Model;
using GraphDemo.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GraphDemo.ViewModel
{
    class GraphViewModel:BindableBase
    {
        private DataRepository rep = new DataRepository();

        //Line style
        private List<int> widthLine =new List<int>();

        public List<int> WidthLine
        {
            get { return widthLine; }
            set
            {
                SetProperty(ref widthLine, value);
            }
        }

        private int currentWidth = 1;
        public int CurrentWidth
        {
            get { return currentWidth; }
            set
            {
                SetProperty(ref currentWidth, value);
            }
        }

        private SolidColorBrush linecolor = new SolidColorBrush();
        public SolidColorBrush LineColor
        {
            get { return linecolor; }
            set
            {
                SetProperty(ref linecolor, value);
            }
        }

        private DoubleCollection stroke = new DoubleCollection() { };
        public DoubleCollection StrokeD
        {
            get { return stroke; }
            set { SetProperty(ref stroke, value); }
        }


        public RelayCommand ColorCommand { get; private set; }

        public RelayCommand LineType1Command { get; private set; }
        public RelayCommand LineType2Command { get; private set; }
        public RelayCommand LineType3Command { get; private set; }

        //Images

        public string ImgSource { get; set; }
        public string ImgSource1 { get; set; }
        public string ImgSource2 { get; set; }

        //Data

        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set
            {
                SetProperty(ref fileName, value);
                DataSet = new ObservableCollection<Data>(rep.GetData(fileName));
            }
        }

        private ObservableCollection<Data> dataSet;
        public ObservableCollection<Data> DataSet
        {
            get { return dataSet; }
            set
            {
                SetProperty(ref dataSet, value);
            }
        }


        public GraphViewModel()
        {
            ColorCommand = new RelayCommand(ColorShow);
            LineType1Command = new RelayCommand(LineType1);
            LineType2Command = new RelayCommand(LineType2);
            LineType3Command = new RelayCommand(LineType3);

            linecolor.Color = System.Windows.Media.Color.FromRgb(0,0,0);

            for (int i = 1; i < 6; ++i)
                widthLine.Add(i);

            ImgSource = "/Images/linetype1.png";
            ImgSource1 = "/Images/linetype2.png";
            ImgSource2 = "/Images/linetype3.png";
        }

       
        private void ColorShow()
        {
            ColorDialog colorDialog = new ColorDialog();
            
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                LineColor.Color = System.Windows.Media.Color.FromRgb(colorDialog.Color.R, colorDialog.Color.G, colorDialog.Color.B);
            }
           
        }

        private void LineType1()
        {
            StrokeD = new DoubleCollection() { };
        }

        private void LineType2()
        {
            StrokeD = new DoubleCollection() { 5,1 };
        }

        private void LineType3()
        {
            StrokeD = new DoubleCollection() { 1 };
        }

    }
}
