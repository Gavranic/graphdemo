﻿using GraphDemo.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace GraphDemo
{
    [ValueConversion(typeof(ObservableCollection<Data>), typeof(PointCollection))]
    class ConvertPoints : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ObservableCollection<Data> values = value as ObservableCollection<Data>;

            if (values == null)
            {
                return new PointCollection();
            }

            PointCollection points = new PointCollection(values.Count);

            foreach (var val in values)
                points.Add(new Point(val.Lat, val.Lgn));

            return points;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
