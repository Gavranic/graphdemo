﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDemo.Model
{
    class Data
    {
        private double lat;
        public double Lat
        {
            get
            {
                return lat;
            }
            set
            {
                lat = value;
            }
        }

        private double lgn;
        public double Lgn
        {
            get
            {
                return lgn;
            }
            set
            {
                lgn = value;
            }
        }

        private double time;
        public double Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        private double vel;
        public double Velocity
        {
            get
            {
                return vel;
            }
            set
            {
                vel = value;
            }
        }

    }
}
